module Test
	puts "Enter a random name (string)"              # This the input taken from user in command line
	name=gets.chomp									 # chomp function is used to remove the trailing space from the string

	number=rand(6..15)                               #rand function generates a random number between a specified range ,for example rand(a..b) will pick random numbers between a and b
	puts "The output is : "+ name +" "+number.to_s   # Integer must be converted to string to be concatenated with string, that's why to_s is used
end

